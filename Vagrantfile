# -*- mode: ruby -*-
# vi: set ft=ruby :
#

ENV['VAGRANT_DEFAULT_PROVIDER'] = 'virtualbox'
Vagrant.configure("2") do |config|
  config.vm.box = "debian/buster64"
  config.vm.synced_folder ".", "/vagrant", disabled: false

  (1..1).each do |i|
    config.vm.define "mainnode#{i}" do |node|
      node.vm.hostname = "mainnode#{i}.crazy"
      node.vm.network "private_network", ip: "10.1.0.1#{i}"
      node.vm.provider :virtualbox do |domain|
        domain.memory = 2048
        domain.cpus = 2
      end
    end
    config.vm.provision "shell", inline: <<-SHELL
    echo "user    ALL= NOPASSWD: /usr/bin/id, /usr/bin/whoami, /bin/sh" >> /etc/sudoers
    echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
    echo "Port 22" >> /etc/ssh/sshd_config
    mkdir /root/.ssh/
    touch /root/.ssh/authorized_keys
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDLDv2DOSxLNsv9prD187ydkSjZ5Be/SdFoKlMoCDwjGrhMu0Cs4Pl1go0GwFOwVO+DhojBUJU4lJupQdSZ3BLKsC1bOLiPlTLQ6ff20ulJoCRoZO/T16/fHhEnvQsTO5HRcxRXL2PNGQrbxNwQfgZIU8RnwU89JMxk6S985H6pHrWnHviMV2XK+0hHXdh2AVim3h7mOAO2AuiqY1kkV+GSm5AHDEY5NZARvg80dDYmVJEEvvpGx0SMJ/bYvv2cBAPIDleuTfYnrhtTIDua+i3HmuvT401fZry15fQfx2LGhQEzuuhbngsmlvXHg9W+cktik8M1+pwVKv8BPP9LunOZo4efHlwL35ExRgdd72MCLVFHCbuzt9tIJ6YRh7GCbMR8GqQ8px1oF0PpBGMAuoWhWD5MTbHvhtfYBRZ6qZJHPXBhw8xHD3dZQwAqnv74pPgczM3CmhG6XOcclr3BfbgYGG5OSdfdQ43QEZpoZlcqtF1VkGOQOv0P/Omt9U7KgkXcGMMtf8PHki59e/jShKhom3Unjhfhj0NrihQ44T2q0BOJw8SQZdxDzGq2IhR666ho3WOpcLRn5OhCZ3bN/d2CXseVPMBK0mtkWaaoDczWEpYxN1Jzx1O6/cOKf4SKNPfezHB34eZDKbibkLYwpfUVjnaX838ODebnP5CKYt2GvQ== cflb@bacteria" >> /root/.ssh/authorized_keys
    service ssh restart
    SHELL
  end

  (1..3).each do |j|
    config.vm.define "node#{j}" do |node|
      node.vm.hostname = "node#{j}.crazy"
      node.vm.network "private_network", ip: "10.1.0.2#{j}"
      node.vm.provider :virtualbox do |domain|
        domain.memory = 1024
        domain.cpus = 1
      end
    end
    config.vm.provision "shell", inline: <<-SHELL
    echo "user    ALL= NOPASSWD: /usr/bin/id, /usr/bin/whoami, /bin/sh" >> /etc/sudoers
    echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
    echo "Port 22" >> /etc/ssh/sshd_config
    mkdir /root/.ssh/
    touch /root/.ssh/authorized_keys
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDLDv2DOSxLNsv9prD187ydkSjZ5Be/SdFoKlMoCDwjGrhMu0Cs4Pl1go0GwFOwVO+DhojBUJU4lJupQdSZ3BLKsC1bOLiPlTLQ6ff20ulJoCRoZO/T16/fHhEnvQsTO5HRcxRXL2PNGQrbxNwQfgZIU8RnwU89JMxk6S985H6pHrWnHviMV2XK+0hHXdh2AVim3h7mOAO2AuiqY1kkV+GSm5AHDEY5NZARvg80dDYmVJEEvvpGx0SMJ/bYvv2cBAPIDleuTfYnrhtTIDua+i3HmuvT401fZry15fQfx2LGhQEzuuhbngsmlvXHg9W+cktik8M1+pwVKv8BPP9LunOZo4efHlwL35ExRgdd72MCLVFHCbuzt9tIJ6YRh7GCbMR8GqQ8px1oF0PpBGMAuoWhWD5MTbHvhtfYBRZ6qZJHPXBhw8xHD3dZQwAqnv74pPgczM3CmhG6XOcclr3BfbgYGG5OSdfdQ43QEZpoZlcqtF1VkGOQOv0P/Omt9U7KgkXcGMMtf8PHki59e/jShKhom3Unjhfhj0NrihQ44T2q0BOJw8SQZdxDzGq2IhR666ho3WOpcLRn5OhCZ3bN/d2CXseVPMBK0mtkWaaoDczWEpYxN1Jzx1O6/cOKf4SKNPfezHB34eZDKbibkLYwpfUVjnaX838ODebnP5CKYt2GvQ== cflb@bacteria" >> /root/.ssh/authorized_keys
    service ssh restart
    SHELL
  end

  (1..1).each do |k|
    config.vm.define "router#{k}" do |node|
      node.vm.hostname = "router#{k}.crazy"
      node.vm.network "private_network", ip: "10.1.0.4#{k}"
      node.vm.provider :virtualbox do |domain|
        domain.memory = 768
        domain.cpus = 1
      end
    end
    config.vm.provision "shell", inline: <<-SHELL
    echo "user    ALL= NOPASSWD: /usr/bin/id, /usr/bin/whoami, /bin/sh" >> /etc/sudoers
    echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
    echo "Port 22" >> /etc/ssh/sshd_config
    mkdir /root/.ssh/
    touch /root/.ssh/authorized_keys
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDLDv2DOSxLNsv9prD187ydkSjZ5Be/SdFoKlMoCDwjGrhMu0Cs4Pl1go0GwFOwVO+DhojBUJU4lJupQdSZ3BLKsC1bOLiPlTLQ6ff20ulJoCRoZO/T16/fHhEnvQsTO5HRcxRXL2PNGQrbxNwQfgZIU8RnwU89JMxk6S985H6pHrWnHviMV2XK+0hHXdh2AVim3h7mOAO2AuiqY1kkV+GSm5AHDEY5NZARvg80dDYmVJEEvvpGx0SMJ/bYvv2cBAPIDleuTfYnrhtTIDua+i3HmuvT401fZry15fQfx2LGhQEzuuhbngsmlvXHg9W+cktik8M1+pwVKv8BPP9LunOZo4efHlwL35ExRgdd72MCLVFHCbuzt9tIJ6YRh7GCbMR8GqQ8px1oF0PpBGMAuoWhWD5MTbHvhtfYBRZ6qZJHPXBhw8xHD3dZQwAqnv74pPgczM3CmhG6XOcclr3BfbgYGG5OSdfdQ43QEZpoZlcqtF1VkGOQOv0P/Omt9U7KgkXcGMMtf8PHki59e/jShKhom3Unjhfhj0NrihQ44T2q0BOJw8SQZdxDzGq2IhR666ho3WOpcLRn5OhCZ3bN/d2CXseVPMBK0mtkWaaoDczWEpYxN1Jzx1O6/cOKf4SKNPfezHB34eZDKbibkLYwpfUVjnaX838ODebnP5CKYt2GvQ== cflb@bacteria" >> /root/.ssh/authorized_keys
    service ssh restart
    SHELL
  end

  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "playbook.yml"
    ansible.groups = {
      "kubernetes" => ["mainnode1", "node1", "node2", "node3", "router1"],
      "master_root" => ["mainnode1"],
      "masters" => ["mainnode1"],
      "workers" => ["node1", "node2", "node3"],
      "router_root" => ["router1"],
      "routers" => ["router1"],
      "storages" => [""]
    }
  end
end
